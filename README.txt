
The Session Favorites module provides a "Favorites" system based soley
on a browser cookie. This allows site visitors to
participate/aggregate favorite content without becoming full-fledged
users.

This module will soon work alongside the Favorite Nodes module, or
possible Views Bookmark (or both), such that once a visitor registers or logs
in, their collected session favorites are transferred to the more
permanent Favorite Nodes or Views Bookmark storage.

Installation
------------
1) Copy the session_favorites folder into your Drupal modules
   directory

2) Enable the module using Administer -> Modules
   (/admin/build/modules)

Configuration
-------------
1) Session Favorites can be configured at Administer -> Settings ->
   Session Favorites (admin/settings/session-favorites)

Support
-------
If you experience a problem with session_favorites or have a problem,
file a request or issue on the session_favorites queue at
http://drupal.org/project/issues/session_favorites. DO NOT POST IN THE
FORUMS.  Posting in the issue queues is a direct line of communication
with the module authors.

No guarantee is provided with this software, no matter how critical
your information, module authors are not responsible for damage caused
by this software or obligated in any way to correct problems you may
experience.

Licensed under the GPL 2.0.
http://www.gnu.org/licenses/gpl-2.0.txt

