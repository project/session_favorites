// $Id

/**
 * @file
 * Session Favorites jquery
 */
Drupal.behaviors.sessionFavorites = function() {
    // add a message container
    var newDiv = $('<div class="session-favorites-message"></div>');

    // convert links to ajax
    $('a.session-favorites-add, a.session-favorites-remove')
      .after(newDiv)
      .click(function () { return Drupal.sessionFavoritesAjax(this); });
}

Drupal.sessionFavoritesAjax = function(link) {
    // paths will be of the form session-favorites/{action}/nid. We
    // need to call session-favorites/js/{action}/nid
    var sFurl = link.href.replace('?','/js?');
    // only proceed if it actually matched
    if (sFurl != link.href) {
	// Make the request
	$.ajax({
	  type: 'GET',
	  url: sFurl,
	  data: '',
	  dataType: 'json',
	  success: function (data) {
            Drupal.sessionFavoritesChangeLink(link, data);          
	    return;
	  },
	  error: function (xmlhttp) {
	    alert('An HTTP error '+ xmlhttp.status +' occured.\n'+ link.href);
	  }
	});

      return false;
    }
}

Drupal.sessionFavoritesChangeLink = function(link, data) {
    if (data.status) {
	// success, display messages
	
	// change link
	link.innerHTML = data.newLinkText;
	// toggle href
	link.href = link.href.replace('session-favorites/' + data.oldLinkTarget, 'session-favorites/' + data.newLinkTarget);
	$(link).next('.session-favorites-message').attr('innerHTML',data.content);
    }
    else {
	alert('booz');
    }

}
